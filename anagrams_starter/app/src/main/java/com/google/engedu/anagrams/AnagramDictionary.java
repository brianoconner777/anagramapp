package com.google.engedu.anagrams;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Random;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Arrays;
import android.util.Log;

public class AnagramDictionary {

    private static final int DEFAULT_WORD_LENGTH = 3;
    private static final int MAX_WORD_LENGTH = 7;
    private Random random = new Random();

    private static HashSet<String> wordSet = new HashSet();
    private static ArrayList<String> wordList = new ArrayList();
    private static HashMap<String, ArrayList> lettersToWord = new HashMap();

    public String helper(String word){
        char[] chars = word.toCharArray();
        Arrays.sort(chars);
        return new String(chars);
    }

    public AnagramDictionary(InputStream wordListStream) throws IOException {
        BufferedReader in = new BufferedReader(new InputStreamReader(wordListStream));
        String line;
        while(null != (line = in.readLine())) {
            String word = line.trim();
            wordSet.add(word);
            wordList.add(word);
            String key = helper(word);
            if(!(lettersToWord.containsKey(key))) {
                ArrayList<String> listWithWord = new ArrayList();
                listWithWord.add(word);
                lettersToWord.put(key, listWithWord);
            }
            else{
                ArrayList arrList = lettersToWord.get(key);
                arrList.add(word);
                lettersToWord.put(key, arrList);
            }
        }

    }

    public boolean validWord(String word){
        return wordSet.contains(word);
    }

    public boolean doesNotContainBase(String base, String word){
        return !(word.toLowerCase().contains(base.toLowerCase()));
    }

    public boolean isGoodWord(String word, String base) {

        return (validWord(word) && doesNotContainBase(base, word));
    }

    public ArrayList<String> getAnagramsWithOneMoreLetter(String word) {
        ArrayList<String> result = new ArrayList<>();
        String wordWithExtraChar = new String();
        for(char i = 'a'; i < 'z' + 1; i++){
            wordWithExtraChar = i + word;
            String key = helper(wordWithExtraChar);

            if(lettersToWord.containsKey(key)){
                result.addAll(lettersToWord.get(key));
            }
        }

        return result;
    }

    public boolean hasFiveAnagrams(String word){
        return lettersToWord.get(helper(word)).size() >= 6;
    }

    public String pickGoodStarterWord() {
        Random r = new Random();
        while(true){
            String word = wordList.get(r.nextInt(wordList.size()));
            if(hasFiveAnagrams(word)){
                return word;
            }
        }
    }
}